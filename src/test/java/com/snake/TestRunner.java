package com.snake;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.snake.database.UserDAOTest;
import com.snake.database.UserTest;
import com.snake.move.PlayerMoveIntegrationTest;
import com.snake.move.PlayerMoveTest;
import com.snake.panel.GameBoardIntegrationTest;
import com.snake.panel.GameBoardTest;
import com.snake.player.LoginTest;
import com.snake.player.RealPlayerTest;
import com.snake.player.SimulatedPlayerTest;

/**
 * Runs all the tests.
 * @author Carmen Grantham
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    UserDAOTest.class,
    UserTest.class,
    PlayerMoveIntegrationTest.class,
    PlayerMoveTest.class,
    GameBoardIntegrationTest.class,
    GameBoardTest.class,
    LoginTest.class,
    RealPlayerTest.class,
    SimulatedPlayerTest.class
})
public class TestRunner {

}