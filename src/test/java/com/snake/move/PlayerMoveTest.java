package com.snake.move;

import java.awt.Color;

import javax.swing.ActionMap;
import javax.swing.InputMap;

import org.junit.Assert;

import org.junit.Test;

import com.snake.player.RealPlayer;

/**
 * Test PlayerMove class
 * @author Carmen Grantham
 *
 */
public class PlayerMoveTest {


    PlayerMoveQueue queue = new PlayerMoveQueue(10);
    Color purple = new Color(51,0,102);
    Color lightPurple = new Color(153,51,255);
    
    InputMap inputMap = new InputMap();
    ActionMap actionMap = new ActionMap();
    RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
    
    
    @Test
    public void performMove_leftToDown_works() {
        // Force player to go left so test of going down works.
        player.direction = Direction.LEFT;
        PlayerMove move = new PlayerMove(player, Direction.DOWN);
        Assert.assertEquals(player.direction, Direction.LEFT);
        
        move.performMove();
        Assert.assertEquals(player.direction, Direction.DOWN);
    }
    

    @Test
    public void performMove_sameDirection_works() {
        // Force player to go left so test of going down works.
        player.direction = Direction.LEFT;
        PlayerMove move = new PlayerMove(player, Direction.LEFT);
        Assert.assertEquals(player.direction, Direction.LEFT);
        
        move.performMove();
        Assert.assertEquals(player.direction, Direction.LEFT);
    }

    @Test
    public void performMove_invalidDirection_ignored() {
        // Force player to go left so test of going down works.
        player.direction = Direction.LEFT;
        // Moving right is invalid so player should remain going left after performMove
        PlayerMove move = new PlayerMove(player, Direction.RIGHT);
        Assert.assertEquals(player.direction, Direction.LEFT);
        
        move.performMove();
        Assert.assertEquals(player.direction, Direction.LEFT);
        
        // Assert player going up cannot go down
        player.direction = Direction.UP;
        move.move = Direction.DOWN;
        move.performMove();
        Assert.assertEquals(player.direction, Direction.UP);
        
        // Assert player going down cannot go up
        player.direction = Direction.DOWN;
        move.move = Direction.UP;
        move.performMove();
        Assert.assertEquals(player.direction, Direction.DOWN);
        
        // Asert player going right cannot go left
        player.direction = Direction.RIGHT;
        move.move = Direction.LEFT;
        move.performMove();
        Assert.assertEquals(player.direction, Direction.RIGHT);
    }
}
