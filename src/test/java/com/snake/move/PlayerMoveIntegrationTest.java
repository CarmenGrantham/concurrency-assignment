package com.snake.move;

import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.snake.player.Player;


/**
 * Integration tests for PlayerMove class.
 * @author Carmen Grantham
 *
 */
public class PlayerMoveIntegrationTest {

    @Test
    public void performMove_LeftThenDown_works() {
        Player mockPlayer = mock(Player.class, RETURNS_DEEP_STUBS);
        mockPlayer.direction = Direction.LEFT;
        Assert.assertEquals(mockPlayer.direction, Direction.LEFT);
        
        PlayerMove playerMove = new PlayerMove(mockPlayer, Direction.UP);
        playerMove.performMove();
        Assert.assertEquals(mockPlayer.direction, Direction.UP);
    }

    @Test
    public void performMove_LeftThenRight_ignored() {
        Player mockPlayer = mock(Player.class, RETURNS_DEEP_STUBS);
        mockPlayer.direction = Direction.LEFT;
        Assert.assertEquals(mockPlayer.direction, Direction.LEFT);
        
        PlayerMove playerMove = new PlayerMove(mockPlayer, Direction.RIGHT);
        playerMove.performMove();
        Assert.assertEquals(mockPlayer.direction, Direction.LEFT);
    }

    @Test
    public void toString_Valid() {
        Player mockPlayer = mock(Player.class, RETURNS_DEEP_STUBS);
        when(mockPlayer.getName()).thenReturn("Harry");        
        
        PlayerMove playerMove = new PlayerMove(mockPlayer, Direction.RIGHT);
        Assert.assertEquals("Harry is moving RIGHT", playerMove.toString());
    }
}
