package com.snake.player;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;

import com.snake.panel.Login;

/**
 * Test the Login class, using mocks where appropriate
 * @author Carmen Grantham
 *
 */
public class LoginTest {
    
    @Test
    public void getUsername_valid() {
        Login mockLogin = mock(Login.class, RETURNS_DEEP_STUBS);
        when(mockLogin.getUsername()).thenReturn("Billy");
        
        Assert.assertEquals("Billy", mockLogin.getUsername());
    }

    @Test
    public void getPassword_valid() {
        Login mockLogin = mock(Login.class, RETURNS_DEEP_STUBS);
        when(mockLogin.getPassword()).thenReturn("secret");
        
        Assert.assertEquals("secret", mockLogin.getPassword());
    }

    @Test
    public void isLoggedIn_notSet_returnsFalse() {
        Login mockLogin = mock(Login.class, RETURNS_DEEP_STUBS);
        Assert.assertFalse(mockLogin.isLoggedIn());
    }
}
