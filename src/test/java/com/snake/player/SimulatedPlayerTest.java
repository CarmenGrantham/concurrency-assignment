package com.snake.player;

import java.awt.Color;

import org.junit.Assert;
import org.junit.Test;

import com.snake.move.Direction;
import com.snake.move.PlayerMoveQueue;

/**
 * Test the SimulatedPlayer class.
 * @author Carmen Grantham
 *
 */
public class SimulatedPlayerTest {

    PlayerMoveQueue queue = new PlayerMoveQueue(10);
    Color purple = new Color(51,0,102);
    Color lightPurple = new Color(153,51,255);
    

    @Test
    public void getName_null_returnsNullName() {
        SimulatedPlayer player = new SimulatedPlayer(null, purple, lightPurple, queue);
        Assert.assertNotNull(player);
        Assert.assertNull(player.getName());
    }

    @Test
    public void getName_valid() {
        String name = "Joe Bloggs";
        SimulatedPlayer player = new SimulatedPlayer(name, purple, lightPurple, queue);
        Assert.assertEquals(name, player.getName());
    }

    @Test
    public void getX_correctValues() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        
        Assert.assertEquals(3, player.getLength());
        
        Assert.assertTrue(player.getX(0) >= 0);
        Assert.assertTrue(player.getX(1) >= 0);
        Assert.assertTrue(player.getX(2) >= 0);
        Assert.assertEquals(-1, player.getX(3));
        Assert.assertEquals(-1, player.getX(10));
        
    }

    @Test
    public void getY_correctValues() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        
        Assert.assertEquals(3, player.getLength());
        
        Assert.assertTrue(player.getY(0) >= 0);
        Assert.assertTrue(player.getY(1) >= 0);
        Assert.assertTrue(player.getY(2) >= 0);
        Assert.assertEquals(-1, player.getY(3));
        Assert.assertEquals(-1, player.getY(10));        
    }

    @Test
    public void addMove_leftRightUp_queueOf3() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        player.addMove(Direction.LEFT);
        Assert.assertEquals(1, queue.getCount());
        player.addMove(Direction.RIGHT);
        Assert.assertEquals(2, queue.getCount());
        player.addMove(Direction.UP);
        Assert.assertEquals(3, queue.getCount());
    }

    @Test
    public void addMove_leftThenBlankThenBlank_queueOf3() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        player.addMove(Direction.LEFT);
        Assert.assertEquals(1, queue.getCount());
        player.addMove();
        Assert.assertEquals(2, queue.getCount());
        player.addMove();
        Assert.assertEquals(3, queue.getCount());
    }
    
    @Test
    public void usePreviousX_valid() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        int previousXPos = player.getX(0);
        player.usePreviousX(1);
        int updatedXPos = player.getX(1);
        Assert.assertEquals(previousXPos, updatedXPos);
    }

    @Test
    public void usePreviousY_valid() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        int previousYPos = player.getY(0);
        player.usePreviousY(1);
        int updatedYPos = player.getY(1);
        Assert.assertEquals(previousYPos, updatedYPos);
    }
    
    @Test
    public void getHeadColor_valid() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        Assert.assertEquals(purple, player.getHeadColor());
    }
    
    @Test
    public void getBodyColor_valid() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        Assert.assertEquals(lightPurple, player.getBodyColor());
    }
    
    @Test
    public void clone_copiedCorrectly() {
        SimulatedPlayer player = new SimulatedPlayer("Joe Bloggs", purple, lightPurple, queue);
        Player clonedPlayer = player.clone();
        Assert.assertTrue(clonedPlayer instanceof SimulatedPlayer);
        Assert.assertEquals(player.getName(), clonedPlayer.getName());
        Assert.assertEquals(player.getHeadColor(), clonedPlayer.getHeadColor());
        Assert.assertEquals(player.getBodyColor(), clonedPlayer.getBodyColor());
    }
}
