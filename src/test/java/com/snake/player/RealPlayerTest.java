package com.snake.player;

import java.awt.Color;

import javax.swing.ActionMap;
import javax.swing.InputMap;

import org.junit.Assert;
import org.junit.Test;

import com.snake.move.Direction;
import com.snake.move.PlayerMoveQueue;

/**
 * Test the RealPlayer class
 * 
 * @author Carmen Grantham
 *
 */
public class RealPlayerTest {

    PlayerMoveQueue queue = new PlayerMoveQueue(10);
    Color purple = new Color(51,0,102);
    Color lightPurple = new Color(153,51,255);
    
    InputMap inputMap = new InputMap();
    ActionMap actionMap = new ActionMap();
    
    @Test
    public void getName_null_returnsNullName() {
        RealPlayer player = new RealPlayer(null, purple, lightPurple, queue, inputMap, actionMap);
        Assert.assertNotNull(player);
        Assert.assertNull(player.getName());
    }

    @Test
    public void getName_valid() {
        String name = "Joe Bloggs";
        RealPlayer player = new RealPlayer(name, purple, lightPurple, queue, inputMap, actionMap);
        Assert.assertEquals(name, player.getName());
    }

    @Test
    public void getX_correctValues() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        
        Assert.assertEquals(3, player.getLength());
        
        Assert.assertTrue(player.getX(0) >= 0);
        Assert.assertTrue(player.getX(1) >= 0);
        Assert.assertTrue(player.getX(2) >= 0);
        Assert.assertEquals(-1, player.getX(3));
        Assert.assertEquals(-1, player.getX(10));
        
    }

    @Test
    public void getY_correctValues() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        
        Assert.assertEquals(3, player.getLength());
        
        Assert.assertTrue(player.getY(0) >= 0);
        Assert.assertTrue(player.getY(1) >= 0);
        Assert.assertTrue(player.getY(2) >= 0);
        Assert.assertEquals(-1, player.getY(3));
        Assert.assertEquals(-1, player.getY(10));        
    }

    @Test
    public void addMove_leftRightUp_queueOf3() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        player.addMove(Direction.LEFT);
        Assert.assertEquals(1, queue.getCount());
        player.addMove(Direction.RIGHT);
        Assert.assertEquals(2, queue.getCount());
        player.addMove(Direction.UP);
        Assert.assertEquals(3, queue.getCount());
    }

    @Test
    public void addMove_leftThenBlankThenBlank_queueOf3() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        player.addMove(Direction.LEFT);
        Assert.assertEquals(1, queue.getCount());
        player.addMove();
        Assert.assertEquals(2, queue.getCount());
        player.addMove();
        Assert.assertEquals(3, queue.getCount());
    }
    
    @Test
    public void usePreviousX_valid() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        int previousXPos = player.getX(0);
        player.usePreviousX(1);
        int updatedXPos = player.getX(1);
        Assert.assertEquals(previousXPos, updatedXPos);
    }

    @Test
    public void usePreviousY_valid() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        int previousYPos = player.getY(0);
        player.usePreviousY(1);
        int updatedYPos = player.getY(1);
        Assert.assertEquals(previousYPos, updatedYPos);
    }
    
    @Test
    public void getHeadColor_valid() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        Assert.assertEquals(purple, player.getHeadColor());
    }
    
    @Test
    public void getBodyColor_valid() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        Assert.assertEquals(lightPurple, player.getBodyColor());
    }
    
    @Test
    public void clone_copiedCorrectly() {
        RealPlayer player = new RealPlayer("Joe Bloggs", purple, lightPurple, queue, inputMap, actionMap);
        Player clonedPlayer = player.clone();
        Assert.assertTrue(clonedPlayer instanceof RealPlayer);
        Assert.assertEquals(player.getName(), clonedPlayer.getName());
        Assert.assertEquals(player.getHeadColor(), clonedPlayer.getHeadColor());
        Assert.assertEquals(player.getBodyColor(), clonedPlayer.getBodyColor());
        
    }
}
