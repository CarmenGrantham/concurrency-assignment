package com.snake.panel;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;

import javax.swing.JPanel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.snake.Main;
import com.snake.player.RealPlayer;

/**
 * Integration tests for the GameBoard class, using mocks where appropriate
 * @author Carmen Grantham
 *
 */
public class GameBoardIntegrationTest {

    GameBoard board;

    @Before
    public void setup() {
        Main mockMain = mock(Main.class, RETURNS_DEEP_STUBS);
        JPanel mockPanel = mock(JPanel.class, RETURNS_DEEP_STUBS);
        board = new GameBoard(mockMain, mockPanel);
    }
    
    @Test
    public void addPlayer_mockPlayer_incrementsSet() {
        RealPlayer player = mock(RealPlayer.class, RETURNS_DEEP_STUBS);
        board.addPlayer(player);
        Assert.assertEquals(1, board.getNumberOfPlayers());
        Assert.assertEquals(1, board.getRealPlayersCount());
        Assert.assertEquals(0, board.getSimulatedPlayersCount());
    }
}
