package com.snake.panel;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;

import java.awt.Color;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JPanel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.snake.Main;
import com.snake.move.PlayerMoveQueue;
import com.snake.player.RealPlayer;
import com.snake.player.SimulatedPlayer;

/**
 * Test the GameBoard class, using mocks where appropriate.
 * @author Carmen Grantham
 *
 */
public class GameBoardTest {
    
    GameBoard board;

    PlayerMoveQueue queue = new PlayerMoveQueue(10);
    Color purple = new Color(51,0,102);
    Color lightPurple = new Color(153,51,255);
    
    InputMap inputMap = new InputMap();
    ActionMap actionMap = new ActionMap();
    
    @Before
    public void setup() {
        Main mockMain = mock(Main.class, RETURNS_DEEP_STUBS);
        JPanel mockPanel = mock(JPanel.class, RETURNS_DEEP_STUBS);
        board = new GameBoard(mockMain, mockPanel);
    }
    

    @Test
    public void addPlayer_RealPlayer_incrementsSet() {
        RealPlayer player = new RealPlayer(null, purple, lightPurple, queue, inputMap, actionMap);
        Assert.assertEquals(0, board.getNumberOfPlayers());
        Assert.assertEquals(0, board.getRealPlayersCount());
        Assert.assertEquals(0, board.getSimulatedPlayersCount());
        board.addPlayer(player);
        Assert.assertEquals(1, board.getNumberOfPlayers());
        Assert.assertEquals(1, board.getRealPlayersCount());
        Assert.assertEquals(0, board.getSimulatedPlayersCount());
    }

    @Test
    public void addPlayer_nullRealPlayer_notAdded() {
        board.addPlayer((RealPlayer)null);
        Assert.assertEquals(0, board.getNumberOfPlayers());
        Assert.assertEquals(0, board.getRealPlayersCount());
        Assert.assertEquals(0, board.getSimulatedPlayersCount());
    }

    @Test
    public void addPlayer_SimulatedPlayer_incrementsSet() {
        SimulatedPlayer player = new SimulatedPlayer(null, purple, lightPurple, queue);
        Assert.assertEquals(0, board.getNumberOfPlayers());
        Assert.assertEquals(0, board.getRealPlayersCount());
        Assert.assertEquals(0, board.getSimulatedPlayersCount());
        board.addPlayer(player);
        Assert.assertEquals(1, board.getNumberOfPlayers());
        Assert.assertEquals(0, board.getRealPlayersCount());
        Assert.assertEquals(1, board.getSimulatedPlayersCount());
    }

    @Test
    public void addPlayer_nullSimulatedPlayer_notAdded() {
        board.addPlayer((SimulatedPlayer)null);
        Assert.assertEquals(0, board.getNumberOfPlayers());
        Assert.assertEquals(0, board.getRealPlayersCount());
        Assert.assertEquals(0, board.getSimulatedPlayersCount());
    }
    
}
