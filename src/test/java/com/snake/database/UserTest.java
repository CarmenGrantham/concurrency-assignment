package com.snake.database;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test the User class.
 * @author Carmen Grantham
 *
 */
public class UserTest {

    @Test
    public void getNameTest() {
        User user = new User("Joe Bloggs", "jbloggs", "password");
        Assert.assertEquals("Joe Bloggs", user.getName());
    }

    @Test
    public void getPasswordTest() {
        User user = new User("Joe Bloggs", "jbloggs", "password");
        Assert.assertEquals("password", user.getPassword());
    }

    @Test
    public void getUsernameTest() {
        User user = new User("Joe Bloggs", "jbloggs", "password");
        Assert.assertEquals("jbloggs", user.getUsername());
    }

}
