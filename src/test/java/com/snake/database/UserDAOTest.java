package com.snake.database;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test the UserDAO class.
 * @author Carmen Grantham
 *
 */
public class UserDAOTest {
    
    UserDAO userDAO = new UserDAO();

    @Test
    public void getUser_validCredentials_found() {
        User user = userDAO.getUser("admin", "admin");
        Assert.assertNotNull(user);
    }

    @Test
    public void getUser_invalidCredentials_returnsNull() {
        User user = userDAO.getUser("INVALID", "WRONG");
        Assert.assertNull(user);
    }

    @Test
    public void getUser_nullCredentials_returnsNull() {
        User user = userDAO.getUser(null, null);
        Assert.assertNull(user);
    }
}
