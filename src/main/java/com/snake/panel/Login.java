package com.snake.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.snake.database.User;
import com.snake.database.UserDAO;

/**
 * The Login popup that prompts for username and password. 
 * 
 * @author Carmen Grantham
 *
 */
public class Login extends JDialog {

    private static final long serialVersionUID = 4925689481874589945L;
    
    private JTextField usernameText;
    private JPasswordField passwordText;
    
    private boolean loggedIn = false;
    
    private User loggedInUser = null;
    
    /**
     * Create the login dialog with Textboxes to enter username and password,
     * and buttons to Login and Cancel the login action.
     * 
     * @param parent The parent JFrame to this Dialog
     */
    public Login(JFrame parent) {
        super(parent, "Login", true);

        // Create a JPanel for the labels and text boxes for username and password
        // to be added to the centre of the dialog 
        JPanel panel = new JPanel(new GridBagLayout());
        
        GridBagConstraints cs = new GridBagConstraints();
        
        cs.fill = GridBagConstraints.HORIZONTAL;
        cs.insets = new Insets(3, 3, 3, 3);
        
        JLabel usernameLabel = new JLabel("Username");
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(usernameLabel, cs);
        
        usernameText = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 2;
        panel.add(usernameText, cs);
        

        JLabel passwordLabel = new JLabel("Password");
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(passwordLabel, cs);
        
        passwordText = new JPasswordField(20);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        panel.add(passwordText, cs);
        
        getContentPane().add(panel, BorderLayout.CENTER);
        
        // Create a JPanel to put the Login and Cancel buttons on to,
        // placed at PAGE_END of the dialog
        JPanel buttonPanel = new JPanel();
        
        JButton loginButton = new JButton("Login");
        loginButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent event) {
                loggedInUser = new UserDAO().getUser(getUsername(), getPassword());
                if (loggedInUser == null) {
                    // User couldn't be found.
                    JOptionPane.showMessageDialog(Login.this, "User not found. Try again", "Login", JOptionPane.ERROR_MESSAGE);
                } else {
                    // User found, so close dialog
                    loggedIn = true;
                    dispose();
                }         
            }
        });
        buttonPanel.add(loginButton);
        
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                // No longer want to log in, so close dialog
                dispose();
            }
        });
        buttonPanel.add(cancelButton);

        getContentPane().add(buttonPanel, BorderLayout.PAGE_END);

        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }
    
    /**
     * Get the username entered in the text field
     * @return Username entered in the text field
     */
    public String getUsername() {
        return usernameText.getText().trim();
    }
 
    /**
     * Get the password entered in the password field
     * @return Password entered in the password field
     */
    public String getPassword() {
        return new String(passwordText.getPassword());
    }
    
    /**
     * Has user been logged in 
     * @return True if user entered a valid username and password
     */
    public boolean isLoggedIn() {
        return loggedIn;
    }
    
    /**
     * Get the logged in user. If login has not occured then it will return null.
     * @return Logged in user, null if not logged in
     */
    public User getLoggedInUser() {
        return loggedInUser;
    }

}
