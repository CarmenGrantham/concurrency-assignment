package com.snake.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.snake.Main;
import com.snake.player.Player;
import com.snake.player.RealPlayer;
import com.snake.player.SimulatedPlayer;

/**
 * Controls the display of the game board. It takes moves from users
 * and shows them on the board.
 * 
 * Code adapted from http://zetcode.com/tutorials/javagamestutorial/snake/
 * @author Carmen Grantham
 *
 */
public class GameBoard extends JPanel {
    
    private static final long serialVersionUID = 1L;
    
    public static final int B_WIDTH = 600;
    public static final int B_HEIGHT = 600;
    public static final int DOT_SIZE = 10;
    public static final int ALL_DOTS = B_WIDTH * B_HEIGHT;
    public static final int RANDOM_X_POSITION = (B_WIDTH / DOT_SIZE) - 1;
    public static final int RANDOM_Y_POSITION = (B_HEIGHT / DOT_SIZE) - 1;
    
    private static final Color APPLE_COLOR = new Color(3,171,14);

    // The x and y coordinate of the apple
    private int apple_x;
    private int apple_y;

    private boolean inGame = true;

       
    private Set<Player> players;        // Only "alive" players
    private int realPlayers = 0;
    private int simulatedPlayers = 0;
      

    private final JLabel realPlayersLabel = new JLabel();
    private final JLabel simulatedPlayersLabel = new JLabel();
    
    private Main game;

    /**
     * Create a GameBoard object and initialise
     */
    public GameBoard(Main game, JPanel summaryPanel) {
        this.game = game;
        initBoard();
        initGame();
        
        // Add labels for number of real and simulated players 
        summaryPanel.add(realPlayersLabel);
        summaryPanel.add(simulatedPlayersLabel);
        updatePlayerLabels();
    }

    /**
     * Initialise the board, setting it as the focus and preferred size
     */
    private void initBoard() {
        setFocusable(true);
        requestFocus();
        requestFocusInWindow();
        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
    }
    
    /**
     * Initialise the game with players and apple location
     */
    private void initGame() {
        // Using CopyOnWriteArraySet a thread safe collection as it will mainly be used for reading.
        players = new CopyOnWriteArraySet<Player>();
        
        locateApple();
    }
    
    /**
     * Add a "real" player to the game.
     * @param player The "real" player to add
     */
    public void addPlayer(RealPlayer player) {
        if (player != null) {
            players.add(player);
            realPlayers++;
            updatePlayerLabels();
        }
    }
    
    /**
     * Add a simulated player to the game.
     * @param player The simulated player to add
     */
    public void addPlayer(SimulatedPlayer player) {
        if (player != null) {
            players.add(player);
            simulatedPlayers++;
            updatePlayerLabels();
        }
    }
    
    /**
     * Get the number of players in the gaem.
     * @return Number of players in the game.
     */
    public int getNumberOfPlayers() {
        return players.size();
    }
    
    /**
     * Get the count of simulated players
     * @return count of simulated players
     */    
    public int getSimulatedPlayersCount() {
        return simulatedPlayers;
    }
    
    /**
     * Get the count of real players
     * @return count of real players
     */    
    public int getRealPlayersCount() {
        return realPlayers;
    }
    
    /**
     * Update the Real Player and Simulated Player labels
     */
    private void updatePlayerLabels() {
        realPlayersLabel.setText("Real Players: " + realPlayers);
        simulatedPlayersLabel.setText("Simulated Players: " + simulatedPlayers);
        System.out.println("Updating labels to Real: " + realPlayers + ", simulated: " + simulatedPlayers);
    }
    
    /**
     * Paint the component so it reflects the moved snakes.
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
    }
    
    /**
     * Draw the game board with the snakes. If game has ended display message.
     * @param g
     */
    private void doDrawing(Graphics g) {

        // Draw the apple on the screen
        drawApple(g);
        
        // Loop through all the players and paint their snakes.
        for (Player player : players) {
            Color headColor = player.getHeadColor();
            Color bodyColor = player.getBodyColor();

            for (int z = 0; z < player.getLength(); z++) {
                if (z == 0) {
                    // Snake Head
                    g.setColor(headColor);
                    g.fillOval(player.getX(z), player.getY(z), DOT_SIZE, DOT_SIZE);
                } else {
                    // Snake Body
                    g.setColor(bodyColor);
                    g.fillOval(player.getX(z), player.getY(z), DOT_SIZE, DOT_SIZE);
                }
            }
        }

        Toolkit.getDefaultToolkit().sync();

        // The game has finished, show the gameOver message
        if (!inGame) {
            gameOver();
        }        
    }

    /**
     * Draw the apple on the screen
     * @param g The graphics engine
     */
    private void drawApple(Graphics g) {
        g.setColor(APPLE_COLOR);
        g.fillOval(apple_x, apple_y, DOT_SIZE, DOT_SIZE);
    }
    
    /**
     * Print message to say game is over and ask to Play Again, or Exit game.
     * Marked as synchronized as many threads may call this method at one time
     * @param g The graphics engine
     */
    private synchronized void gameOver() {
        
        // Only show Game Over when not in game.
        if (!inGame) {
            
            Object[] options = {"Play Again", "Exit Game"};

            // Ask user if they want to play again or exit game
            int answer = JOptionPane.showOptionDialog(this, "Game Over!\n\nWhat would you like to do?", "Snakes Game", 
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
            
            if (answer == 0) {
                // Play again
                System.out.println("Play Again");
                restartGame();
            } else {
                // Exit game by calling shutdown on Game object
                System.out.println("Exit Game");
                game.shutdown();
            }
        }
        
        
    }
    
    /**
     * Restart the game by resetting all snakes to default length.
     */
    private synchronized void restartGame() {
        inGame = true;
        realPlayers = 0;
        simulatedPlayers = 0;
        players.removeAll(players);
        
        // Restart the game
        game.restart(this);
        
        updatePlayerLabels();
        
        // Add Apple to game
        locateApple();
    }

    /**
     * Check if apple has been eaten. If so calculate a new position for the apple.
     */
    private void checkApple() {
        for (Player player : players) {
            if (player.ateApple(apple_x, apple_y)) {
                locateApple();
            }
        }
    }

    /**
     * Automatically move snake in current direction, this is to cater when players
     * don't press a key.
     */
    private void move() {
        for (Player player : players) {
            for (int z = player.getLength(); z > 0; z--) {
                player.usePreviousY(z);
                player.usePreviousX(z);
            }
            
            player.move();
        }
    }

    /**
     * Check if any snakes hit themselves or another snake.
     */
    private synchronized void checkCollision() {
        for (Player player : players) {
            int headX = player.getX(0);
            int headY = player.getY(0);
    
            for (int z = player.getLength(); z > 0; z--) {
    
                // Check if player hit's itself
                if ((z > 4) && (headX == player.getX(z)) && (headY == player.getY(z))) {
                    System.out.println("Player " + player.getName() + " has hit itself.");
                    removePlayer(player);
                    
                }
            }
            
            // Check if head of this snake has collided with any part of another snake.
            for (Player otherPlayer : players) {
                // Only check other players, if same player ignore.
                if (!otherPlayer.equals(player)) {
                    for (int z = otherPlayer.getLength(); z > 0; z--) {
                        if (headX == otherPlayer.getX(z) && headY == otherPlayer.getY(z)) {
                            // There was a collisions
                            System.out.println("There's been a collision between " + player.getName() + " and " + otherPlayer.getName() + "!");
                            removePlayer(player);
                            removePlayer(otherPlayer);
                        }
                    }
                }
            }
        }
    }
    
    private void removePlayer(Player player) {
        
        if (player instanceof RealPlayer) {
            realPlayers--;
            // End the game if real player, and stop moving other snakes. 
            inGame = false;
            for (Player p : players) {
                p.dead();
            }
        } else {
            player.dead();
            players.remove(player);
            simulatedPlayers--;
        }
        updatePlayerLabels();
    }
    

    /**
     * Generate a random position for the apple
     */
    private void locateApple() {

        int r = (int) (Math.random() * RANDOM_X_POSITION);
        apple_x = ((r * DOT_SIZE));

        r = (int) (Math.random() * RANDOM_Y_POSITION);
        apple_y = ((r * DOT_SIZE));
    }
    
    /**
     * Update the game board.
     */
    public synchronized void updateBoard() {
        if (inGame) {
            checkApple();
            checkCollision();
            move();
        }
        repaint();
    }
    
}
