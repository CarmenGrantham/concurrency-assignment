package com.snake.player;

import java.awt.Color;

import com.snake.move.PlayerMoveQueue;


/**
 * The simulated player. Move the snake in random directions at random times.
 * 
 * @author Carmen Grantham
 *
 */
public class SimulatedPlayer extends Player implements Runnable {
    
    /**
     * Create a SimulatedPlayer object.
     * @param name The name of the player
     * @param headColor The snake's head color.
     * @param bodyColor The snake's body color.
     * @param moveQueue The queue to place moves
     */
    public SimulatedPlayer(String name, Color headColor, Color bodyColor, PlayerMoveQueue moveQueue) {
        super(name, headColor, bodyColor, moveQueue);
    }

    @Override
    public Player clone() {
        return new SimulatedPlayer(getName(), getHeadColor(), getBodyColor(), moveQueue);
    }

    /**
     * Implement Runnable interface.
     * Move the snake in a random direction at random times.
     */
    @Override
    public void run() {
        // Set name of thread to help with identification later
        Thread.currentThread().setName("SimPlayer-" + getName() + "-Thread");

        while (running) {
            delay();
            randomMove();
        }
    }
    
    /**
     * Direct the snake in a random direction. 
     */
    private void randomMove() {
        addMove(getRandomDirection());
    }

    /**
     * Pause the moving of the snake for a random length of time.
     */
    public static void delay() {
        int actualDelay;
        
        try {
            // thread to sleep for random milliseconds
            actualDelay = randomWithRange(0, 1000);
            Thread.sleep(actualDelay);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    /**
     * Generate a random value between min and max
     * @param min The minimum random value.
     * @param max The maximum random value.
     * @return A random value between min and max
     */
    private static int randomWithRange(int min, int max) {
        int range = (max-min) + 1;
        return (int) (Math.random() * range) + min;
    }
    
}
