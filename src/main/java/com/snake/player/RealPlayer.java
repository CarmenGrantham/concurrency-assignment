package com.snake.player;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;

import com.snake.move.Direction;
import com.snake.move.PlayerMoveQueue;

/**
 * The "real" player playing the snakes game.
 * 
 * @author Carmen Grantham
 *
 */
public class RealPlayer extends Player {

    private static final String LEFT = "Left";
    private static final String RIGHT = "Right";
    private static final String UP = "Up";
    private static final String DOWN = "Down";
    
    private static final int DELAY = 140;
    
    private Direction newDirection = null;
    
    private InputMap inputMap;
    private ActionMap actionMap;
        
    /**
     * Create a RealPlayer object.
     * @param name The name of the player
     * @param headColor The snake's head color.
     * @param bodyColor The snake's body color.
     * @param moveQueue The queue to place moves
     * @param inputMap The inputMap to capture key presses
     * @param actionMap The actionMap to determine action on key presses.
     */
    public RealPlayer(String name, Color headColor, Color bodyColor, PlayerMoveQueue moveQueue, InputMap inputMap, ActionMap actionMap) {
        super(name, headColor, bodyColor, moveQueue);
        this.inputMap = inputMap;
        this.actionMap = actionMap;
        addKeyBindings(inputMap, actionMap);
    }
    
    /**
     * The actions to take when keys UP, DOWN, LEFT, RIGHT are clicked.
     * @param inputMap The inputMap to capture key presses
     * @param actionMap The actionMap to determine action on key presses.
     */
    private void addKeyBindings(InputMap inputMap, ActionMap actionMap) {

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), LEFT);
        actionMap.put(LEFT, new MoveAction(Direction.LEFT));
        
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), RIGHT);
        actionMap.put(RIGHT, new MoveAction(Direction.RIGHT));
            
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), UP);
        actionMap.put(UP, new MoveAction(Direction.UP));
        
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), DOWN);
        actionMap.put(DOWN, new MoveAction(Direction.DOWN));

    }
    
    @Override
    public Player clone() {
        return new RealPlayer(getName(), getHeadColor(), getBodyColor(), moveQueue, inputMap, actionMap);
    }


    /**
     * A class for action to take when key pressed.
     * @author Carmen Grantham
     *
     */
    private class MoveAction extends AbstractAction {
        private static final long serialVersionUID = 2768415381076512178L;
        
        Direction direction;
        
        MoveAction(Direction direction) {
            this.direction = direction;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            newDirection = direction;
        }
    }
    
    /**
     * Implement Runnable interface.
     * Keep snake moving by going in same direction.
     */
    @Override
    public void run() {
        // Set name of thread to help with identification later
        Thread.currentThread().setName("RealPlayer-" + getName() + "-Thread");
        while (running) {
            if (newDirection != null) {
                addMove(newDirection);
                newDirection = null;
            } else {
                // Keep snake moving in same direction when no input provided
                addMove();
            }
            // Pause thread
            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
