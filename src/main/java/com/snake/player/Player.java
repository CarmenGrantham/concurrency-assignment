package com.snake.player;

import java.awt.Color;
import java.util.Arrays;
import java.util.Objects;

import com.snake.move.Direction;
import com.snake.move.PlayerMove;
import com.snake.move.PlayerMoveQueue;
import com.snake.panel.GameBoard;

/**
 * The superclass for players in the Snake Game.
 * 
 * @author Carmen Grantham
 *
 */
public abstract class Player implements Runnable{

    private String name;
    
    // All player moves are put into a queue 
    protected PlayerMoveQueue moveQueue;
    
    private Color headColor;
    private Color bodyColor;

    // Initialise length of snake
    private int length = 3;
    
    // The x coordinates of the snake. x[0] is the location of the snake's head.
    private final int x[];
    // The y coordinates of the snake.
    private final int y[];
    // The current direction of the snake
    public Direction direction;

    protected boolean running = true;
    
    public abstract Player clone();
    
    /**
     * Create a Player object.
     * @param name The person's name
     * @param headColor The snake's head colour.
     * @param bodyColor The snake's body colour.
     * @param moveQueue The queue where player moves are stored.
     */
    public Player(String name, Color headColor, Color bodyColor, PlayerMoveQueue moveQueue) {
        this.name = name;
        this.headColor = headColor;
        this.bodyColor = bodyColor;
        this.moveQueue = moveQueue;
        
        x = new int[GameBoard.ALL_DOTS];
        y = new int[GameBoard.ALL_DOTS];
        
        // Set all values to -1 rather than 0 as 0 is a valid value, so to test for valid values
        // use [value] >= 0
        Arrays.fill(x, -1);
        Arrays.fill(y, -1);
        
        initSnake();
    }
    
    /**
     * Initialise the random positioning and direction of the snake
     */
    private void initSnake() {
        
        // Initialise Snake to random position and direction 
        int headPositionX = getRandomX();
        int headPositionY = getRandomY();
        x[0] = headPositionX * GameBoard.DOT_SIZE;
        y[0] = headPositionY * GameBoard.DOT_SIZE;
        
        this.direction = getRandomDirection();
                
        switch(direction) {
        case LEFT:
            for (int i = 1; i < length; i++) {
                x[i] = headPositionX + (i * GameBoard.DOT_SIZE);
                y[i] = headPositionY;
            }
            break;
        case RIGHT:
            for (int i = 1; i < length; i++) {
                x[i] = headPositionX - (i * GameBoard.DOT_SIZE);
                y[i] = headPositionY;
            }
            break;
        case UP:
            for (int i = 1; i < length; i++) {
                x[i] = headPositionX;
                y[i] = headPositionY + (i * GameBoard.DOT_SIZE);
            }
            break;
        case DOWN:
            for (int i = 1; i < length; i++) {
                x[i] = headPositionX;
                y[i] = headPositionY - (i * GameBoard.DOT_SIZE);
            }
            break;
        }
    }
    
    /**
     * Get the name of the player
     * @return Player's name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Generate a random x coordinate
     * @return Random x coordinate
     */
    private int getRandomX() {
        return (int) (Math.random() * GameBoard.RANDOM_X_POSITION);
    }
    
    /**
     * Generate a random y coordinate
     * @return Random y coordinate
     */
    private int getRandomY() {
        return (int) (Math.random() * GameBoard.RANDOM_Y_POSITION);
    }
    
    /**
     * Generate a random direction (LEFT, RIGHT, UP, DOWN)
     * @return A random direction
     */
    protected Direction getRandomDirection() {
        int randomDirection = (int) (Math.random() * Direction.values().length);
        return Direction.values()[randomDirection];
    }
    
    /**
     * Get the length of the snake
     * @return Snake's length
     */
    public int getLength() {
        return length;
    }
    
    /**
     * Get the x coordinate at the provided index
     * @param index The index to get x coordinate for
     * @return The x coordinate at the index
     */
    public int getX(int index) {
        return x[index];
    }
    
    /**
     * Get the y coordinate at the provided index
     * @param index The index to get y coordinate for
     * @return The y coordinate at the index
     */
    public int getY(int index) {
        return y[index];
    }
    
    /**
     * As the snake moves it uses the same coordinate as the snake piece before it.
     * So if the snake head (index = 0) has x coordinate of 20, then index = 1 will 
     * then be given value of 20 when it moves.
     * @param index The index to use
     */
    public void usePreviousX(int index) {
        x[index] = x[index - 1];
    }
    
    /**
     * As the snake moves it uses the same coordinate as the snake piece before it.
     * So if the snake head (index = 0) has y coordinate of 20, then index = 1 will 
     * then be given value of 20 when it moves.
     * @param index The index to use
     */
    public void usePreviousY(int index) {
        y[index] = y[index - 1];
    }
    
    /**
     * Create a new move in the same direction snake is already going.
     */
    protected void addMove() {
        addMove(direction);
    }
    
    /**
     * Create a new move in the specified direction
     * @param move The direction to go in
     */
    protected void addMove(Direction move) {
        moveQueue.add(new PlayerMove(this, move));
    }
    
    /**
     * The colour of the snakes head to use. This is the value at index 0
     * @return Snake's head colour
     */
    public Color getHeadColor() {
        return headColor;
    }
    
    /**
     * The colour of the snake's body to use. This is for all values > 0
     * @return Snake's body colour
     */
    public Color getBodyColor() {
        return bodyColor;
    }
    
    /**
     * Checks if snake has eaten apple, ie Snakes head is in same position as the apple 
     * @param apple_x The x coordinate of the apple
     * @param apple_y The y coordinate of the apple
     * @return True if snake's head is in same position as apple
     */
    public boolean ateApple(int apple_x, int apple_y) {
        boolean match = (x[0] == apple_x) && (y[0] == apple_y);
        if (match) {
            length++;
        }
        return match;
    }
    
    /**
     * The player has been killed so stop the Thread
     */
    public void dead() {
        running = false;
    }
    
    /**
     * Move the snake, using the saved direction
     */
    public void move() {
        switch (direction) {
        case LEFT:
            moveLeft();
            break;
        case RIGHT:
            moveRight();
            break;
        case UP:
            moveUp();
            break;
        case DOWN:
            moveDown();
            break;
        }
    }

    /**
     * Move the snake left, wrapping it to opposite side of board if necessary
     */
    private void moveLeft() {
        x[0] -= GameBoard.DOT_SIZE;
        // If snake runs off board wrap to opposite side
        if (x[0] < 0) {
            x[0] = GameBoard.B_WIDTH - GameBoard.DOT_SIZE;
        }
    }
    
    /**
     * Move the snake right, wrapping it to opposite side of board if necessary
     */
    private void moveRight() {
        x[0] += GameBoard.DOT_SIZE;
        // If snake runs off board wrap to opposite side
        if (x[0] >= GameBoard.B_WIDTH) {
            x[0] = 0;
        }
    }
    
    /**
     * Move the snake up, wrapping it to opposite side of board if necessary
     */
    private void moveUp() {
        y[0] -= GameBoard.DOT_SIZE;
        
        // If snake runs off board wrap to opposite side
        if (y[0] < 0) {
            y[0] = GameBoard.B_HEIGHT - GameBoard.DOT_SIZE;
        }        
    }
    
    /**
     * Move the snake down, wrapping it to opposite side of board if necessary
     */
    private void moveDown() {
        y[0] += GameBoard.DOT_SIZE;

        if (y[0] >= GameBoard.B_HEIGHT) {
            y[0] = 0;
        }
    }    
    
    /**
     * Override the equals method. The objects are the same if they are a Player
     * with the same name.
     */
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        
        if (!(o instanceof Player)) {
            return false;
        }
        
        Player player = (Player) o;
        return Objects.equals(name, player.name);
    }
    
    /**
     * Override the hashCode method.
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
