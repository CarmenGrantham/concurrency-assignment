package com.snake;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.snake.database.User;
import com.snake.move.PlayerMoveQueue;
import com.snake.panel.GameBoard;
import com.snake.panel.Login;
import com.snake.player.Player;
import com.snake.player.RealPlayer;
import com.snake.player.SimulatedPlayer;
import com.snake.server.GameServer;

/**
 * Starts the Snakes Game. Sets up the board (with JFrame) and automatically
 * adds 4 simulated players to the board and starts them moving in random
 * directions. The "Real Player" can they join the game after logging in.
 * @author Carmen Grantham
 *
 */
public class Main implements Runnable {

    // All players in the game, even those killed - Used when user decides to play game again
    private final Set<Player> allPlayers = new CopyOnWriteArraySet<Player>();     
    
    // The number of Simulated Players to add to the game
    private static final int SIMULATED_PLAYERS_COUNT = 4;
    
    private ExecutorService threadPool;
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Main());
    }
    
    /**
     * Create the Game object.
     */
    public Main() {
        // Use thread pool of size 10 because there will be 4 SimulatedPlayer's, 1 Real Player
        // and 2 GameServers (1 for Simulated and 1 for Real), for total of 7.
        this.threadPool = Executors.newFixedThreadPool(10);;
    }
    
    /**
     * Shutdown the game. First need to shutdown the ExecutorService, wait a little while,
     * then exit the application.
     */
    public void shutdown() {
        System.out.println("Shutting down the Game....");
        try {
            threadPool.shutdownNow();
            threadPool.awaitTermination(100, TimeUnit.MICROSECONDS);

        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        System.out.println("Game shutdown cleanly.");
        System.exit(0);
    }
    
    /**
     * Execute the Game. This builds a JFrame with the Game board on it.
     */
    @Override
    public void run() {
        final JFrame frame = new JFrame("Snakes");
        
        // Player summary area
        final JPanel summaryPanel = new JPanel();
        frame.add(summaryPanel, BorderLayout.SOUTH);
        
        
        // Join Game section
        final JPanel joinPanel = new JPanel();
        frame.add(joinPanel, BorderLayout.NORTH);
        
        // The Game board
        final GameBoard gameBoard = new GameBoard(this, summaryPanel);
        addSimulatedPlayers(gameBoard, SIMULATED_PLAYERS_COUNT);
        frame.add(gameBoard, BorderLayout.CENTER);
        
        // Add "Join Game" button for real players to log in to the game
        final JButton joinButton = new JButton("Join Game");
        joinButton.addActionListener(joinGameListener(frame, gameBoard, joinButton));
        joinPanel.add(joinButton);
        
        
        // When window is closed call the shutdown() method
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                shutdown();
            }
        });
        
        // Cannot change size of window
        frame.setResizable(false);
        
        frame.pack();

        // Make frame visible
        frame.setVisible(true);
    }
    
    /**
     * Create a listener to the "Join Game" button click, to show Login
     * Dialog.
     * @param frame The JFrame to use
     * @param gameBoard The GameBoard 
     * @param joinButton The JButton to join the game
     * @return ActionListener that will load the Login Dialog when button is clicked
     */
    private ActionListener joinGameListener(final JFrame frame, final GameBoard gameBoard, final JButton joinButton) {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Login login = new Login(frame);
                login.setVisible(true);
                if (login.isLoggedIn()) {
                    User user = login.getLoggedInUser();
                    System.out.println("Successfully logged in as " + user.getName());
                    addRealPlayer(gameBoard, user);
                    // Only one real player allowed in game so disable join button
                    joinButton.setEnabled(false);
                }
            }
        };
    }
    

    
    /**
     * Add a real player to the game. It has it's own GameServer to handle it's moves.
     * 
     * @param name The player's name
     */
    private void addRealPlayer(GameBoard gameBoard, User user) {

        // The Real Player put it's moves in PlayerMoveQueue that are processed by GameServer 1
        PlayerMoveQueue moveQueue1 = new PlayerMoveQueue(10);
        
        GameServer gameServer1 = new GameServer(gameBoard, moveQueue1);
        
        Color purple = new Color(51,0,102);
        Color lightPurple = new Color(153,51,255);
        RealPlayer realPlayer = new RealPlayer(user.getName(), purple, lightPurple, moveQueue1, 
                gameBoard.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW), gameBoard.getActionMap());
        gameBoard.addPlayer(realPlayer);
        allPlayers.add(realPlayer);
        
        threadPool.submit(gameServer1);
        threadPool.submit(realPlayer);
        
    }
    
    /**
     * Add simulated players to the game. One GameServer handles moves of all simulated players.
     * 
     * @param numberSimulatedPlayers The number of simulated players to add
     */
    private void addSimulatedPlayers(GameBoard gameBoard, int numberSimulatedPlayers) {
        
        // Simulated players use their own PlayerMoveQueue and GameServer
        // so GameServer may be processing up to 4 simulated players
        PlayerMoveQueue moveQueue2 = new PlayerMoveQueue(10);
        GameServer gameServer2 = new GameServer(gameBoard, moveQueue2);

        // Create the simulated players and add them to the game board
        for (int i = 1; i <= numberSimulatedPlayers; i++) {
            String name = "Simulated Player " + i;
            addSimulatedPlayer(gameBoard, name, moveQueue2);
        }
        // One GameServer thread to use for all simulated players
        threadPool.submit(gameServer2);
    }
    

    /**
     * Add a simulated player
     * 
     * @param name The simulated player's name
     * @param moveQueue Where player moves are stored for processing
     */
    private void addSimulatedPlayer(GameBoard gameBoard, String name, PlayerMoveQueue moveQueue) {
        // Simulated players have grey head, and light grey body
        Color grey = new Color(32,32,32);
        Color lightGrey = new Color(160,160,160);

        SimulatedPlayer simulatedPlayer = new SimulatedPlayer(name, grey, lightGrey, moveQueue);
        gameBoard.addPlayer(simulatedPlayer);
        allPlayers.add(simulatedPlayer);
        
        // Create Simulated Player thread using the same name as the simulated player
        threadPool.submit(simulatedPlayer);
    }
    
    /**
     * Restart the game. Recreate all players and restart their threads.
     * @param gameBoard The gameBoard to add the players to
     */
    public synchronized void restart(GameBoard gameBoard) {
        // Create a copy of all players in the game and recreate them in new Threads
        Set<Player> clonedPlayers = new HashSet<Player>();
        for (Player player : allPlayers) {
            Player clonedPlayer = player.clone();
            threadPool.submit(clonedPlayer);
            clonedPlayers.add(clonedPlayer);
        }
        allPlayers.removeAll(allPlayers);
        
        // Add the cloned players into the game
        for (Player clonedPlayer : clonedPlayers) {
            if (clonedPlayer instanceof RealPlayer) {
                gameBoard.addPlayer((RealPlayer)clonedPlayer);
            } else {
                gameBoard.addPlayer((SimulatedPlayer)clonedPlayer);
            }
            allPlayers.add(clonedPlayer);
        }        

    }
}
