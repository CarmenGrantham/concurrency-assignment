package com.snake.move;

import com.snake.player.Player;

/**
 * Object to represent moves by the player.
 * 
 * @author Carmen Grantham
 *
 */
public class PlayerMove {
    
    public Player player;
    public Direction move;

    /**
     * Create the PlayerMove object.
     * 
     * @param player The player to use
     * @param move The move to use
     */
    public PlayerMove(Player player, Direction move) {
        this.player = player;
        this.move = move;
    }
    
    /**
     * Execute the move by the player
     */
    public void performMove() {
        // Only move the player if valid direction. If invalid the move is ignored.
        if (isValidMove()) {
            player.direction = move;
        }
    }
    
    /**
     * Check if move is valid. Players cannot move snake in opposite
     * direction they are currently going in. 
     * ie. When going left player cannot move right.
     * 
     * @return True if move is valid
     */
    private boolean isValidMove() {
        Direction currentMove = player.direction;
        if (currentMove == move) {
            return true;
        }
        
        // Cannot move in opposite direction
        // Eg. If going Left cannot go right, need to go Up or Down first to go Left
        switch (currentMove) {
        case LEFT:
            return move != Direction.RIGHT;
        case RIGHT:
            return move != Direction.LEFT;
        case UP:
            return move != Direction.DOWN;
        case DOWN:
            return move != Direction.UP;
        default:
            return false;
        }
    }
    
    public String toString() {
        return player.getName() + " is moving "  + move;
    }
}
