package com.snake.move;


/**
 * Store all the Player Moves.
 * This works like a Bounded Buffer so that actions occur in 
 * a thread safe manner.
 * 
 * @author Carmen Grantham
 *
 */
public class PlayerMoveQueue {
    
    // Size of queue
    private int size;
    
    private PlayerMove[] moves;
    
    // The index to add new items to in the moves array
    private int inIndex = 0;
    
    // The index to use when removing items from moves array
    private int outIndex = 0;
    
    // Number of items in buffer
    private int count = 0;

    /**
     * Create PlayerMoveQueue object
     * @param size The size of the PlayerMove queue to use
     */
    public PlayerMoveQueue(int size) {
        this.size = size;
        moves = new PlayerMove[size];
    }
    
    /**
     * Add a PlayerMove to the queue.
     * If the queue is full it will wait until there is available.
     * 
     * @param playerMove The PlayerMove to add to the queue
     */
    public synchronized void add(PlayerMove playerMove) {
        // If buffer full, cannot add to it
        while (count == size) {
            // block thread as buffer is full
            try {
                this.wait();
                
            } catch (InterruptedException ex){}
        }
        
        // Add value to array
        moves[inIndex] = playerMove;
        inIndex = (inIndex + 1) % size;
        count++;
        
        // Notify all other threads that they can check for items to add
        notifyAll();
    }
    
    /**
     * Remove a PlayerMove from the queue.
     * If there are no items in the queue it will wait until one
     * is available to return.
     * 
     * @return The removed player queue
     */
    public synchronized PlayerMove remove() {
        // if queue is empty wait until there is something to fetch
        while (count == 0) {
            try {
                wait();
            } catch (InterruptedException ex) {}
        }
        
        // Get the player move from the moves array
        PlayerMove move = moves[outIndex];
        outIndex = (outIndex + 1) % size;
        count--;
        
        // Notify all other threads that they can check for items to remove
        notifyAll();
        return move;
    }
    
    /**
     * Get the count for the buffer, ie number of moves.
     * @return Number of moves in the buffer.
     */
    public int getCount() {
        return count;
    }
}
