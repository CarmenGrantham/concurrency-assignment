package com.snake.move;

/**
 * The enumerator for snake direction.
 * @author Carmen Grantham
 *
 */
public enum Direction {
    UP, 
    DOWN, 
    LEFT, 
    RIGHT
}
