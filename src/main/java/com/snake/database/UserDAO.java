package com.snake.database;

import java.util.concurrent.ConcurrentNavigableMap;


/**
 * The User Data Access Object that enables users to login.
 * @author Carmen Grantham
 *
 */
public class UserDAO extends BaseDAO {

    public static final String TABLE_NAME = "users";
    
    /**
     * Get the user with this username and password. 
     * If either of these is null then null is returned.
     * It uses case sensitive matching.
     * 
     * @param username The username to use
     * @param password The password to use
     * @return User with matching username and password, otherwise null
     */
    public synchronized User getUser(String username, String password) {
        
        if (username != null) {
            ConcurrentNavigableMap<String, User> map = getDatabase().getTreeMap(UserDAO.TABLE_NAME);
            User user = map.get(username);
            if (user != null) {
                if (password != null && password.equals(user.getPassword())) {
                    return user;
                }
            }
        }
        return null;
    }

}
