package com.snake.database;

import java.io.File;

import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * Super class for all Database objects. It handles connection to the database.
 * @author Carmen Grantham
 */
public class BaseDAO {    

    private final static String DB_NAME = "snakes";
    private DB db;
    
    /**
     * Create the DatabaseCreator object and connect to the database.
     */
    public BaseDAO() {
        db = DBMaker.newFileDB(new File(DB_NAME))
                .closeOnJvmShutdown()
                .encryptionEnable("password")
                .make();
    }
    
    public DB getDatabase() {
        return db;
    }
}
