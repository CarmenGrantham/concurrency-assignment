package com.snake.database;

import java.io.Serializable;

/**
 * A User object that logs in to the Snake Game
 * 
 * @author Carmen Grantham
 */
public class User implements Serializable {

    private static final long serialVersionUID = 6505634233595352584L;
    
    private String name;
    private String username;
    private String password;
    
    /**
     * Create a username with the given name, username and password
     * @param name The name to use
     * @param username The username to use
     * @param password The password to use
     */
    public User(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
    }

    /**
     * Get the login's name
     * @return The login's name
     */
    public String getName() {
        return name;
    }
    
    /**
     * Get the login's username
     * @return The login's username
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * Get the login's password
     * @return The login's password
     */
    public String getPassword() {
        return password;
    }
}
