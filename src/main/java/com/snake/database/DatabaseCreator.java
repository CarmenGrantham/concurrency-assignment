package com.snake.database;

import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Create the database and populate it with some players.
 * 
 * @author Carmen Grantham
 */
public class DatabaseCreator extends BaseDAO {
    
    public static void main(String[] args) {
        DatabaseCreator dbCreator = new DatabaseCreator();
        dbCreator.createUsers();
        dbCreator.printUsers();
    }

    /**
     * Create some users in the database
     */
    public void createUsers() {
        // Open existing collection (or create new)
        ConcurrentNavigableMap<String, User> map = getDatabase().getTreeMap(UserDAO.TABLE_NAME);
        
        map.put("admin", new User("Administrator", "admin", "admin"));
        map.put("jbloggs", new User("Joe Bloggs", "jbloggs", "abc123!"));

        getDatabase().commit();
        
    }
    
    /**
     * Print details of users in the database
     */
    public void printUsers() {
        ConcurrentNavigableMap<String, User> map = getDatabase().getTreeMap(UserDAO.TABLE_NAME);
        System.out.println("1. descendingKeySet: " + map.descendingKeySet() + "\n");
        System.out.println("2. descendingMap: " + map.descendingMap() + "\n");
        System.out.println("3. get['admin']: " + map.get("admin") + "\n");
    }
}
