package com.snake.server;

import com.snake.move.PlayerMove;
import com.snake.move.PlayerMoveQueue;
import com.snake.panel.GameBoard;

/**
 * The GameServer that handles player moves.
 * 
 * @author Carmen Grantham
 *
 */
public class GameServer implements Runnable {
    
    private GameBoard gameBoard;
    private PlayerMoveQueue moveQueue;

    /**
     * Create the GameServer object
     * @param gameBoard The GameBoard to update
     * @param moveQueue The PlayerMoveQueue to get player moves from
     */
    public GameServer(GameBoard gameBoard, PlayerMoveQueue moveQueue) {
        this.gameBoard = gameBoard;
        this.moveQueue = moveQueue;
    }
    
    /**
     * Implement Runnable interface.
     * Processes player moves from the moveQueue and then triggers the update
     * of the game board.
     */
    @Override
    public void run() {
        // Set name of thread to help with identification later
        Thread.currentThread().setName("GameServerThread");

        while(true) {
            PlayerMove playerMove = moveQueue.remove();
            playerMove.performMove();
            gameBoard.updateBoard();
        }
    }
}
